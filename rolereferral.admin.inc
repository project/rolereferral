<?php

drupal_add_css(drupal_get_path("module", "rolereferral") ."/css/rolereferral.admin.css", "module", "all", FALSE);
drupal_add_js(drupal_get_path("module", "rolereferral") ."/js/rolereferral.admin.js");

/**
 * Role Referral Settings - allows administrator to identify accepted
 * referring URLs, and what roles they are to be granted
 */
function rolereferral_admin($form_state) {
  global $user;

  $form['refer_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Current Allowed Referring Sites') ."<br/>",
    '#description' => t('Click on a link to modify the allowed referral'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#weight' => 0
  );

  $roles = user_roles();

  $referer_table = variable_get($rolereferral_referer_table, NULL);

  if ($referer_table != NULL) {

    foreach ($referer_table as $id => $url_and_role) {

      $title = $url_and_role['url'] ." is granted role: ". $roles[$url_and_role['role']];

      $form['refer_list'][$id] = array(
        '#prefix' => '<div>',
        '#suffix' => '</div>', 
        '#value' => l($title, "admin/settings/rolereferral/edit/". $id)
      );
    }
  }

  $form['new_refer'] = array(
    '#type' => 'fieldset',
    '#title' => t('URL referral role assignment') ."<br/>",
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#weight' => 0
  );

  $form['new_refer']['url'] = array(
    '#prefix' => '<div class="floatLeft">',
    '#type' => 'textfield',
    '#title' => 'URL',
    '#size' => '50',
    '#required' => TRUE,
    '#suffix' => '</div>',
  );
  $form['new_refer']['seperator'] = array(
    '#prefix' => '<div class="floatLeft">',
    '#value' => '&nbsp; &nbsp; &nbsp; &nbsp;',
    '#suffix' => '</div>',
  );
  $form['new_refer']['role'] = array(
    '#prefix' => '<div class="floatLeft">',
    '#type' => 'select',
    '#title' => t('grant Role'),
    '#options' => $roles,
    '#description' => "select a role to grant to the referral",
    '#suffix' => '</div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  $form['#submit'][] = 'rolereferral_submit';

  return $form;
}


function rolereferral_submit($form, &$form_values) {

  $referer_table = variable_get($rolereferral_referer_table, NULL);

    // NOTE: we allow the referer table to index itself, so that every pairing
    // has a unique ID (allowing for multiple roles for a given url and vice
    // versa), so we store the url and role in their own little two-key array.

  if ($referer_table == NULL) {
    $referer_table = array(array('url' => $form_values['values']['url'],
                                 'role' => $form_values['values']['role']));
  }
  else {
    $referer_table[] = array('url' => $form_values['values']['url'], 
                             'role' => $form_values['values']['role']);
  }

  variable_set($rolereferral_referer_table, $referer_table);

}


function rolereferral_admin_edit($arg, $arg1, $arg3) {

  $referer_table = variable_get($rolereferral_referer_table, NULL);
  if ($referer_table == NULL) {
    error_log("PROBLEM: rolereferral_admin_edit was called when $rolereferral_referer_table was empty.");
    header("Location: /admin/settings/rolereferral");
    exit;
  }

  $url_and_role = $referer_table[$arg3];
  if (!isset($url_and_role)) {
    drupal_set_message("Please select your role-referral again");
    header("Location: /admin/settings/rolereferral");
    exit;
  }

  $form['info_to_edit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Referral role assignment'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#weight' => 0
  );

  $form['info_to_edit']['url'] = array(
    '#prefix' => '<div class="floatLeft">',
    '#type' => 'textfield',
    '#title' => 'Referral URL',
    '#size' => '50',
    '#default_value' => $url_and_role['url'],
    '#required' => TRUE,
    '#suffix' => '</div>'
  );

  $form['info_to_edit']['seperator'] = array(
    '#prefix' => '<div class="floatLeft">',
    '#value' => '&nbsp; &nbsp; &nbsp; &nbsp;',
    '#suffix' => '</div>'
  );

  $form['info_to_edit']['role'] = array(
    '#prefix' => '<div class="floatLeft">',
    '#type' => 'select',
    '#title' => t('grant role'),
    '#options' => user_roles(),
    '#default_value' => $url_and_role['role'],
    '#description' => "Select a role to grant the referring URL",
    '#suffix' => '</div>'
  );

  $form['info_to_edit']['delete'] = array(
    '#prefix' => '<div class="floatLeft" id="deleteLink">',
    '#value' => '&nbsp; &nbsp; &nbsp; &nbsp;'. l("delete", "admin/settings/rolereferral/delete/". $arg3),
    '#suffix' => '</div>',
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $arg3
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['cancel'] = array(
    '#prefix' => '<div class="floatLeft">',
    '#value' => '<input type="button" value="Cancel" id="cancel">',
    '#suffix' => '</div>'
  );
    
  $form['#submit'][] = 'rolereferral_edit_submit';
  $form['#redirect'] = 'admin/settings/rolereferral';

  return $form;
}


function rolereferral_edit_submit($form, &$form_values) {

  $referer_table = variable_get($rolereferral_referer_table, NULL);

  $referer_table[$form_values['values']['id']] = array(
    'url' => $form_values['values']['url'],
    'role' => $form_values['values']['role']
  );

  variable_set($rolereferral_referer_table, $referer_table);

}

function rolereferral_admin_delete($arg, $arg1, $arg3) {

  $referer_table = variable_get($rolereferral_referer_table, NULL);
  if ($referer_table == NULL) {
    error_log("PROBLEM: rolereferral_admin_delete was called when $rolereferral_referer_table was empty.");
    header("Location: /admin/settings/rolereferral");
    exit;
  }

  $url_and_role = $referer_table[$arg3];
  if (!isset($url_and_role)) {
    drupal_set_message("Please select your role-referral again");
    header("Location: /admin/settings/rolereferral");
    exit;
  }

  $roles = user_roles();

  $title = "<li>Referral from URL: ". $url_and_role['url'] ." grants role: ". $roles[$url_and_role['role']];

  $form['markup'] = array(
    '#prefix' => "<div id='confirmbox'>",
    '#suffix' => "",
    '#value' => "Are you certain that you wish to remove this role-referral? <ul>". $title ."</ul>"
  );

  $form['accept'] = array(
    '#type' => 'submit',
    '#value' => 'Delete',
    '#prefix' => '<div class="floatLeft">',
    '#suffix' => '</div>'
  );

  $form['seperator'] = array(
    '#prefix' => '<div class="floatLeft">',
    '#value' => '&nbsp; &nbsp; &nbsp; &nbsp;',
    '#suffix' => '</div>'
  );

  $form['cancel'] = array(
    '#prefix' => '<div class="floatLeft">',
    '#value' => '<input type="button" value="Cancel" id="cancel">',
    '#suffix' => '</div>'
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $arg3
  );

  $form['#submit'][] = 'rolereferral_delete_submit';
  $form['#redirect'] = 'admin/settings/rolereferral';
  return $form;
}

function rolereferral_delete_submit($form, &$form_values) {

  $referer_table = variable_get($rolereferral_referer_table, NULL);
  unset($referer_table[$form_values['values']['id']]);

  // re-index table
  $referer_table = array_values($referer_table);

  variable_set($rolereferral_referer_table, $referer_table);
  drupal_set_message("The role-referral has been deleted.");
}
